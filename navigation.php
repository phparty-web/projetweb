<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 20/04/2017
 * Time: 17:51
 */
session_start(); ?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <a class="navbar-brand" href="newsFeed.php">Sway</a>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b
                        class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <!-- ADD FRIEND -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b
                        class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <?php
                    require_once("scripts/dbConnect.php");

                    $verification = $conn->query('SELECT iduser,name FROM friendrequest,user WHERE idreceveur = ("' . $_SESSION['id'] . '") AND iddemandeur=iduser');

                    while ($donnees = mysqli_fetch_assoc($verification)) {

                        $demande = $donnees['name'];
                        $idami = $donnees['iduser'];
                        echo "   <form role=\"form\" action=\"scripts/addFriend.php\" method=\"post\">
                                    <span class=\"label label-success\">Friend Request From</span>" . " " . $demande . "
                                        <input type=\"hidden\" value=" . $idami . " name='idami'/>
                                        <input type=\"submit\" value=\"Add\" class=\"notifBtn \" />
                                    </form>
                                    <li class=\"divider\"></li>";
                    }

                    ?>
                </li>
                <li>
                    <a href="friends.php" style="text-align:center; font-size: 9pt">Add a new Friend</a>
                </li>
            </ul>
        </li>
        <!-- EVENTS -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <b
                        class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <?php
                    require_once("scripts/dbConnect.php");

                    $verification2 = $conn->query('SELECT name,idevent FROM invevent,user WHERE idinvite = ("' . $_SESSION['id'] . '") AND idcreator=iduser');

                    while ($donnees = mysqli_fetch_assoc($verification2)) {

                        $demande = $donnees['name'];
                        $idevent = $donnees['idevent'];
                        echo "   <form role=\"form\" action=\"#\" method=\"post\">
                                    <span class=\"label label-primary\">Event Invite From</span>" . " " . $demande . "
                                        <input type=\"hidden\" value=" . $idevent . " name='idami'/>
                                        <input type=\"submit\" value=\"Add\" class=\"notifBtn \" />
                                    </form>
                                    <li class=\"divider\"></li>";
                    }

                    ?>
                </li>
                <li>
                    <a href="events.php" style="text-align:center; font-size: 9pt">Create an Event</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                <?php
                // On récupère nos variables de session
                if (isset($_SESSION['pseudo'])) {
                    echo 'Hello ' . $_SESSION['pseudo'];
                }
                ?>
                <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="reglages.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="scripts/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="newsFeed.php"><i class="fa fa-fw fa-dashboard"></i> News Feed</a>
            </li>
            <li>
                <a href="events.php"><i class="fa fa-fw fa-calendar"></i> Events</a>
            </li>
            <li>
                <a href="photo.php"><i class="fa fa-fw fa-camera"></i> Pictures</a>
            </li>
            <li>
                <a href="myDiscussions.php"><i class="fa fa-fw fa-comment"></i> Messenger</a>
            </li>

            <?php
            require_once('scripts/getAdmin.php');
            if ($type == "Admin") {
                echo "
                <li>
                    <a href=\"admin.php\"><i class=\"fa fa-fw fa-cogs\"></i> Bail D'admins</a>
                </li>";
            }
            ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>