<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 16/04/2017
 * Time: 22:01
 */
// On démarre la session
session_start();

// On détruit les variables de notre session
session_unset();

// On détruit notre session
session_destroy();

// On redirige le visiteur vers la page d'accueil
echo '<script type="text/javascript">
    window.location.replace("../index.php")
    </script>';
?>