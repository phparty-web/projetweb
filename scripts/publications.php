<?php session_start(); ?>
<?php

require_once("dbConnect.php");

$iduser = $GLOBALS['id'];
$name = $GLOBALS['name'];
$photo = $GLOBALS['photo'];
$publications = null;

if ($GLOBALS['typeFeed'] === "profile") {
    $publications = $conn->query(' SELECT * FROM publication WHERE idpublieur = ' . $iduser . ' ORDER BY idpublication DESC');
} elseif ($GLOBALS['typeFeed'] === "newsfeed") {
    $publications = $conn->query(' SELECT idpublication,idpublieur,type,content,location,date,feeling,access FROM publication, friend WHERE (idpublieur=iduser1 and ' . $iduser . '=iduser2)or (idpublieur=iduser2 and iduser1=' . $iduser . ') UNION SELECT * FROM publication where idpublieur=' . $iduser . ' ORDER BY idpublication DESC');
}

$nb_publication = 0;

while ($donnees = mysqli_fetch_assoc($publications)) {


    //récupération des valeurs de l'user qui a publié
    $idpublicateur = $donnees['idpublieur'];

    $requeteDonneesUser = $conn->query(" SELECT * FROM user WHERE iduser=" . $idpublicateur . " ");
    while ($donneePublicateur = mysqli_fetch_assoc($requeteDonneesUser)) {
        $pub_iduser = $donneePublicateur['iduser'];
        $pub_name = $donneePublicateur['name'];
        $pub_pseudo = $donneePublicateur['pseudo'];
        $pub_email = $donneePublicateur['email'];
        $pub_pic = $donneePublicateur['picture'];
        $pub_cover = $donneePublicateur['cover'];
        $pub_desc = $donneePublicateur['description'];
    }


    //on récupère l'id de la publication
    $idpubli = $donnees['idpublication'];
    if ($donnees['type'] == 1) {
        $photos = $conn->query(' SELECT path FROM upload,publication WHERE id_publication = ' . $idpubli . '');
        while ($donne = mysqli_fetch_assoc($photos)) {

            $cheminPhoto = $donne['path'];
        }

    }


    $dejaReagi = false;
    $numeroReaction = -1;

    // Partie reactions
    $reponseReaction = $conn->query('SELECT iduser,name,numero FROM user,reaction WHERE idpublic=("' . $idpubli . '") and idreacteur =iduser ');
    $nombreReaction = 0;
    while ($donneee = mysqli_fetch_assoc(($reponseReaction))) {
        $nombreReaction = $nombreReaction + 1;

        if ($iduser == $donneee['iduser']) {
            if ($dejaReagi === true) {
                $nombreReaction--;
            }
            $dejaReagi = true;
            $numeroReaction = $donneee['numero'];
        }
    }

    if ($donnees['feeling'] == 0) {
        $sentiments = "Happy :)";
    }
    if ($donnees['feeling'] == 1) {
        $sentiments = "Excited :o";
    }
    if ($donnees['feeling'] == 2) {
        $sentiments = "Tired";
    }
if ($donnees['feeling'] == 3) {
    $sentiments = "Mad :'(";
} else $sentiments = "Chill :*";

    //Corps de la publication
    include 'blockhtml/publication.html';
    ?>
    <script>
        var id_pub = "pub" + <?php echo($nb_publication);?>;

        $("#new").attr("id", id_pub);
        id_pub = "#" + id_pub;


        $(id_pub).find(".profile_pic2").attr("src", "<?php echo($pub_pic);?>");
        $(id_pub).find(".name").text("<?php echo($pub_name);?>");
        $(id_pub).find(".user-link").attr("href", "profile.php?ref=" + "<?php echo($pub_iduser);?>");

        $(id_pub).find(".feeling").text("<?php echo "est " . ($sentiments);?>");
        $(id_pub).find(".description").text("<?php echo($donnees['content']);?>");
        $(id_pub).find(".publication_pic").attr("src", "<?php echo($cheminPhoto);?>");

        $(id_pub).find(".num-likes").text("<?php echo($nombreReaction);?>" + " reacted");
        <?php if($dejaReagi === TRUE){ ?>

        $(id_pub).find(".form-reactions").find(".reactions_but").removeClass("reaction_but_small reaction_but_big").addClass("reaction_but_small");
        $(id_pub).find(".form-reactions").find(".reac" + "<?php echo($numeroReaction);?>").addClass("reaction_but_big");
        <?php }

        else{?>
        $(id_pub).find(".form-reactions").find(".reactions_but").addClass("reaction_but_small");
        <?php }?>

        $(id_pub).find(".form-reactions").find(".reactions_but").hover(function () {
            if ($(this).hasClass("reaction_but_big") == false) {
                $(this).addClass("reaction_but_hover");
            }

        }, function () {
            $(this).removeClass("reaction_but_hover");
        });
        $(id_pub).find(".id-publi").attr("value", "<?php echo($idpubli);?>");
        $(id_pub).find(".id-publi-list").attr("value", "<?php echo($nb_publication);?>");

    </script>
    <?php
    //Chargement de tous le commentaires par la publication en question
    //$commentaires = $conn->query(' SELECT name,content,picture,date FROM user,comment WHERE idpubli = ' . $publi . ' AND iduser=idcommenteur');
    $commentaires = $conn->query("SELECT `user`.`name` , `user`.`iduser`, `comment`.`content`, `user`.`picture`, `comment`.`date`, `publication`.`idpublication`  
                                         FROM `user`, `comment`, `publication` 
                                          WHERE `comment`.`idpubli` =" . $idpubli . "  AND `publication`.`idpublication` =" . $idpubli . " AND `user`.`iduser`=`comment`.`idcommenteur`");

    $nb_comment = 0;
    while ($donnee = mysqli_fetch_assoc($commentaires)) {
        include 'blockhtml/comment.html';
        $photo3 = $donnee['picture'];
        $name2 = $donnee['name'];
        ?>

        <script>
            var id_comment = "comment" + <?php echo($nb_comment);?>;

            $("#new_comment").attr("id", id_comment);
            id_comment = "#" + id_comment;
            $(id_pub).find(id_comment).find(".profile_pic2").attr("src", "<?php echo($photo3);?>");
            $(id_pub).find(id_comment).find(".name2").text("<?php echo($name2);?>");
            $(id_pub).find(id_comment).find(".content").text("<?php echo($donnee['content']);?>");

            $(id_pub).find(id_comment).find(".user-link").attr("href", "profile.php?ref=" + "<?php echo($donnee['iduser']);?>");



        </script>

        <?php
        $nb_comment++;
    }

    include 'blockhtml/newComment.html';
    ?>
    <script>
        $(id_pub).find(".add-comment").find(".profile_pic2").attr("src", "<?php echo($photo);?>");
        $(id_pub).find(".add-comment").find(".addcomment_id").attr("value", "<?php echo($idpubli);?>");

    </script>
    <?php


    $nb_publication++;
}


?>



















