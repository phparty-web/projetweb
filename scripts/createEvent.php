<?php

session_start();
$iduser = $_SESSION['id'];
$name = $_POST['name'];
$location = $_POST['location'];
$startDate = $_POST['eventStart'];
$endDate = $_POST['eventEnd'];
$table = 'events';
$db = 'phparty';

require_once('dbConnect.php');

//verify
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function insertTable($db, $table, $id, $name, $startDate, $endDate, $location)
{
    $insert = "INSERT into " . $table . " (idcreator, name, start, end, location) VALUES(?,?,?,?,?)";


    $stmt = $db->prepare($insert);
    if (!$stmt->bind_param("issss", $id, $name, $startDate, $endDate, $location))
        die("Blindage error: " . $stmt->error);
    if (!$stmt->execute())
        die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
}


if (!empty($_POST['name']) AND !empty($_POST['location'])) {
    insertTable($conn, $table, $iduser, htmlspecialchars($_POST['name']),
        htmlspecialchars($_POST['eventStart']),
        htmlspecialchars($_POST['eventEnd']),
        htmlspecialchars($_POST['location']));
    $success = "Event created !";
    echo '<script type="text/javascript">
    window.location.replace("../events.php")
    </script>';

    exit();
}


?>