<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway - Admin</title>

    <!-- Personnal CSS -->
    <link href="../css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 21/04/2017
 * Time: 00:16
 */
$username = $_POST['name'];
$pseudo = $_POST['pseudo'];
$passe = htmlspecialchars($_POST['pass1']);
$passe2 = htmlspecialchars($_POST['pass2']);
$email = $_POST['email'];
$table = 'user';
$db = 'phparty';

require_once('dbConnect.php');

function insertTable($db, $table, $username, $pseudo, $email, $password)
{
    $insert = "INSERT into " . $table . " (name, pseudo, email, password) VALUES(?,?,?,?)";


    $stmt = $db->prepare($insert);
    if (!$stmt->bind_param("ssss", $username, $pseudo, $email, $password))
        die("Blindage error: " . $stmt->error);
    if (!$stmt->execute())
        die("Execution failed: (" . $stmt->errno . ") " . $stmt->error);
}


if ($passe == $passe2) {

    if (!empty($_POST['name']) AND !empty($_POST['pass1']) AND !empty($_POST['pass2'])) {
        insertTable($conn, $table, htmlspecialchars($_POST['name']), htmlspecialchars($_POST['pseudo']), htmlspecialchars($_POST['email']), sha1(htmlspecialchars($_POST['pass1'])));
        $pass = sha1($passe);
        $sql = "Select * from `user` where `email` = '$email' and `password` = '$pass' ";

        $tab = mysqli_fetch_array(mysqli_query($conn, $sql));

        $result = array();
        array_push($result,
            array("id" => $tab['iduser'],
                "name" => $tab['name'],
                "pseudo" => $tab['pseudo'],
                "email" => $tab['email'],
                "password" => $tab['password'],
                "picture" => $tab['picture'],
                "cover" => $tab['cover'],
                "description" => $tab['description'],
                "type" => $tab['type']));

        echo '<script type="text/javascript">
    window.location.replace("../admin.php")
    </script>';
        exit();
    }
    echo '<script type="text/javascript">
    window.location.replace("../admin.php")
    </script>';
    exit();
} else {
    $matchErr = "The two passwords do not match";
    include('../admin.php');
}


?>
