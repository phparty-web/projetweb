<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 21/04/2017
 * Time: 02:12
 */
session_start();
//DELETE CACHE
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="../img/logo/ic_loopr.ico">

    <title>Sway - Messenger</title>

    <!-- Personnal CSS -->
    <link href="../css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <a class="navbar-brand" href="../newsFeed.php">Sway</a>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b
                            class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <!-- ADD FRIEND -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b
                            class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <?php
                        require_once("dbConnect.php");

                        $verification = $conn->query('SELECT iduser,name FROM friendrequest,user WHERE idreceveur = ("' . $_SESSION['id'] . '") AND iddemandeur=iduser');

                        while ($donnees = mysqli_fetch_assoc($verification)) {

                            $demande = $donnees['name'];
                            $idami = $donnees['iduser'];
                            echo "   <form role=\"form\" action=\"addFriend.php\" method=\"post\">
                                    <span class=\"label label-success\">Friend Request From</span>" . " " . $demande . "
                                        <input type=\"hidden\" value=" . $idami . " name='idami'/>
                                        <input type=\"submit\" value=\"Add\" class=\"notifBtn \" />
                                    </form>
                                    <li class=\"divider\"></li>";
                        }

                        ?>
                    </li>
                    <li>
                        <a href="../friends.php" style="text-align:center; font-size: 9pt">Add a new Friend</a>
                    </li>
                </ul>
            </li>
            <!-- EVENTS -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <b
                            class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <?php
                        require_once("dbConnect.php");

                        $verification2 = $conn->query('SELECT name,idevent FROM invevent,user WHERE idinvite = ("' . $_SESSION['id'] . '") AND idcreator=iduser');

                        while ($donnees = mysqli_fetch_assoc($verification2)) {

                            $demande = $donnees['name'];
                            $idevent = $donnees['idevent'];
                            echo "   <form role=\"form\" action=\"#\" method=\"post\">
                                    <span class=\"label label-primary\">Event Invite From</span>" . " " . $demande . "
                                        <input type=\"hidden\" value=" . $idevent . " name='idami'/>
                                        <input type=\"submit\" value=\"Add\" class=\"notifBtn \" />
                                    </form>
                                    <li class=\"divider\"></li>";
                        }

                        ?>
                    </li>
                    <li>
                        <a href="../events.php" style="text-align:center; font-size: 9pt">Create an Event</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                    <?php
                    // On récupère nos variables de session
                    if (isset($_SESSION['pseudo'])) {
                        echo 'Hello ' . $_SESSION['pseudo'];
                    }
                    ?>
                    <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="../profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="../reglages.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="../newsFeed.php"><i class="fa fa-fw fa-dashboard"></i> News Feed</a>
                </li>
                <li>
                    <a href="../events.php"><i class="fa fa-fw fa-calendar"></i> Events</a>
                </li>
                <li>
                    <a href="../photo.php"><i class="fa fa-fw fa-camera"></i> Pictures</a>
                </li>
                <li>
                    <a href="../myDiscussions.php"><i class="fa fa-fw fa-comment"></i> Messenger</a>
                </li>

                <?php
                require_once('../scripts/getAdmin.php');
                if ($type == "Admin") {
                    echo "
                <li>
                    <a href=\"../admin.php\"><i class=\"fa fa-fw fa-cogs\"></i> Bail D'admins</a>
                </li>";
                }
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">
            <?php


            require_once('dbConnect.php');
            $iduser = $_SESSION['id'];
            $name = $_SESSION['name'];
            $pic = $_SESSION['photo'];
            $iddiscuss = $_POST['iddiscussion'];
            $myMessages = $conn->query('SELECT * FROM messages WHERE idconv = ' . $iddiscuss . '');
            //AFFICHAGE DES MESSAGES
            while ($donnees = mysqli_fetch_assoc($myMessages)) {
                if ($donnees['iduser'] == $iduser) {
                    echo "<div class=\"publication well\">
                            <IMG SRC =" . "../" . $pic . " class=\"messPic\">
                            <strong>" . $name . ": " . "</strong>" . $donnees['message'] . "<br>
                          </div>";
                } else {
                    $myMessages2 = $conn->query('SELECT name,picture FROM user WHERE iduser = ' . $donnees['iduser'] . '');
                    while ($donneesAmi = mysqli_fetch_assoc($myMessages2)) {
                        echo "<div class=\"publication well\">
                            <IMG SRC =" . "../" . $donneesAmi['picture'] . " class=\"messPic\">
                            <strong>" . $donneesAmi['name'] . ":" . "</strong>" . $donnees['message'] . "<br>
                          </div>";
                    }

                }

            }
            echo "<form role=\"form\" action=\"addReply.php\" method=\"post\" >
                <div class=\"row\">
                    <div class=\"form-group\">
                        <label></label>
                        <input type='text' class=\"form-control\" placeholder=\"Reply...\" name=\"reply\" >

                    </div>
                </div>
                <input type=\"hidden\" value=" . $iddiscuss . " name=\"iddiscussion\">
                <div class=\"row\">
                    <div class=\"form-group\">
                        <div class='actions'>
                        <button type='submit'  class='btn btn-primary'>Submit</button>
                        </div>
                        

                    </div>
                </div>";

            ?>
            <script src="../js/disable.js"></script>
        </div>
    </div>

    <!-- /.container-fluid -->
        <?php
        include('../footer.html');
        ?>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../js/plugins/morris/raphael.min.js"></script>
<script src="../js/plugins/morris/morris.min.js"></script>
<script src="../js/plugins/morris/morris-data.js"></script>

</body>

</html>