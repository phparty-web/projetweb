<?php
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_name = "phparty";

//connect server
$conn = new mysqli($db_servername, $db_username, $db_password);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//connect the database
if (!$conn->query("USE " . $db_name)) {
    echo "Error using database: " . $conn->error;
}


?>