<?php
/*
 * Type publication:
 * 0 - texte
 * 1 - photo
 * 2 - video
 * 3 - document
 */

session_start();
$iduser = $_SESSION['id'];
$type_publication = $GLOBALS['type_publication'];
$desc_publication = $GLOBALS['desc_publication'];
$location = $GLOBALS['location'];
$feeling = $GLOBALS['feeling'];
$access = $GLOBALS['access'];


require_once('dbConnect.php');

//Calcule $id_publication pour upload.php
$sql = "SELECT idpublication FROM publication ORDER BY idpublication DESC";
$res = mysqli_query($conn, $sql);

$row = mysqli_fetch_array($res);
$id_publication = $row['idpublication'];
$id_publication++;


if (($type_publication === 0) && empty($desc_publication)) {

    /* Publication de texte mais pas de contenu, erreur */
    echo('<script> alert(\'No text or file in publication\'); </script>');
    mysqli_close($conn);
    echo '<script type="text/javascript">
    window.location.replace("../newsFeed.php")
    </script>';
    exit();
} else {

    $desc_publication = htmlspecialchars($desc_publication);
    $location = htmlspecialchars($location);


    $sql = "INSERT INTO publication (idpublieur, type, content, location, date, feeling, access) VALUES 
                    ('$iduser', '$type_publication', '$desc_publication', '$location', CURRENT_TIMESTAMP, '$feeling', '$access')";

    if ($conn->query($sql) === FALSE) {
        echo('<script> alert(\'Unable to publish for now. Please try again later\\nError: mysql connection \'); </script>');
    }

    if ($type_publication === 0) {
        mysqli_close($conn);
        echo '<script type="text/javascript">
    window.location.replace("../newsFeed.php")
    </script>';
        exit();
    }
}


?>