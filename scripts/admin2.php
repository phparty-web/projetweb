<h1>User List</h1>
<?php
session_start();
require_once("dbConnect.php");
$iduser = $_SESSION['id'];
$userlist = $conn->query(' SELECT iduser, name, pseudo, email, picture, admin FROM user');
echo $matchErr;
echo " 
 <form style='margin-bottom: 15px;' class='col-lg-12' role=\"form\" method='post' action='scripts/createUser.php'>
                <div class=\"row col-lg-8\">
                    <div class=\"form-group\">
                        <label>Last Name</label>
                        <input class=\"form-control\" placeholder=\"Enter your last name\" name=\"name\" id=\"name\">

                    </div>
                </div>
                <div class=\"row col-lg-8\">
                    <div class=\"form-group\">
                        <label>Pseudo</label>
                        <input class=\"form-control\" placeholder=\"Enter your pseudo\" name=\"pseudo\" id=\"pseudo\">

                    </div>
                </div>
                <div class=\"row col-lg-8\">
                    <div class=\"form-group\">
                        <label>Email</label>
                        <input class=\"form-control\" placeholder=\"Enter your email\" name=\"email\" id=\"email\">

                    </div>
                </div>
                <div class=\"row col-lg-8\">
                    <div class=\"form-group\">
                        <label>Password</label>
                        <input class=\"form-control\" type=\"password\" name=\"pass1\" id=\"pass1\">

                    </div>
                </div>
                <div class=\"row col-lg-8\">
                    <div class=\"form-group\">
                        <label>Please confirm your password</label>
                        <input class=\"form-control\" type=\"password\" name=\"pass2\" id=\"pass2\">

                    </div>
                </div>
                <div class=\"row col-lg-8\">
                    <div class=\"actions\">
                        <input type=\"submit\" value=\"Create\" class=\"btn btn-default\" disabled=\"disabled\">
                    </div>
                </div>
        </form>";

while ($donnees = mysqli_fetch_assoc($userlist)) {
    //Si ce n'est pas un admin on ne l'affiche pas
    $idtodelete = $donnees['iduser'];
    if (!$donnees['admin']) {
        echo "  <div class=\"row\">
                    <ul class='list-group3'>
                        <li class='list-group-item3'>
                        
                            <form method='post' action='scripts/deleteUser.php'>
                                <input type=\"hidden\" value=" . $idtodelete . " name=\"idtodelete\">
                              <button type='submit' style='float: right;' class='btn btn-danger' ><i class=\"fa fa-fw fa-times\" ></i></button>
                            </form>
                            <IMG SRC =" . $donnees['picture'] . " class=\"friendPic2\"> " . $donnees['name'] . "
                            <br>
                            <br>
                            " . $donnees['pseudo'] . "
                            <br>
                            <br>
                            " . $donnees['email'] . "
                        </li>
                    </ul>
                </div>
                ";
    }

}
?>
