<?php
/*
 * 3 types d'upload:
 * upload -> publication avec image video ou fichier - pas de description
 * profilepicture -> Profile pic - pas de publication
 * coverpicture -> Cover pic - pas de publication
 */

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    session_start();

    $uploaddir = "null";
    $desc_upload = $_POST['content'];
    $GLOBALS['desc_publication'] = $desc_upload;
    $GLOBALS['location'] = $_POST['location'];
    $GLOBALS['feeling'] = $_POST['feeling'];
    $GLOBALS['access'] = $_POST['access'];
    $GLOBALS['type_publication'] = 0;
    $profilePic = $_POST['pic'];
    $iduser = $_SESSION['id'];

    //Si fichier chargé
    if ($_FILES['file']['size'] > 0) {

        switch ($_POST['type']) {
            case 'upload':

                //Pas de description du fichier dans upload mais dans sa publication
                $desc_upload = "";

                //Determine le chemin selon le format
                switch ($_FILES['file']['type']) {
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/gif':
                        $uploaddir = 'uploads/images/';
                        $type_publication = 1;
                        break;

                    case 'video/mpeg':
                    case 'video/quicktime':
                    case 'video/x-msvideo':
                    case 'video/3gpp':
                        $uploaddir = 'uploads/videos/';
                        $type_publication = 2;
                        break;

                    default:
                        $uploaddir = 'uploads/docs/';
                        $type_publication = 3;
                        break;
                }

                $GLOBALS['type_publication'] = $type_publication;
                //Cree la publication et initialise $id_publication
                require_once('createPublication.php');

                break;
            case 'profilepicture':
                switch ($_FILES['file']['type']) {
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/gif':
                        $uploaddir = 'img/profilepics/';
                        break;
                    default:
                        $uploaddir = 'null';

                }

                break;

            case 'coverpicture':
                switch ($_FILES['file']['type']) {
                    case 'image/png':
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/gif':
                        $uploaddir = 'img/coverpics/';
                        break;
                    default:
                        $uploaddir = 'null';
                }
                break;
        }

        if ($uploaddir !== "null") {

            require_once('dbConnect.php');

            $sql = "SELECT id FROM upload ORDER BY id DESC";

            $res = mysqli_query($conn, $sql);
            $row = mysqli_fetch_array($res);
            $id = $row['id'];

            $id++;

            //Découpe du fichier pour déterminer son extension
            $filename = basename($_FILES['file']['name']);
            $filename = explode('.', $filename);
            $ext = $filename[count($filename) - 1];


            //Chemin du fichier
            $path = $uploaddir . $id . '.' . $ext;
            $id_user = $_SESSION['id'];

            $sql = "INSERT INTO upload (id_user, id_publication,path,date_time, type, description) VALUES ('$id_user', '$id_publication','$path', CURRENT_TIMESTAMP, '$ext', '$desc_upload')";

            if (mysqli_query($conn, $sql)) {

                if (move_uploaded_file($_FILES['file']['tmp_name'], "../" . $path)) {

                    switch ($_POST['type']) {
                        case 'upload':
                            echo '<script type="text/javascript">
                                window.location.replace("../newsFeed.php");
                                </script>';
                            break;
                        case 'profilepicture':
                            break;
                        case 'coverpicture':
                            break;

                    }



                } else {
                    echo "ERROR\n";
                    echo 'Voici quelques informations de débogage :';
                    print_r($_FILES);

                }

            }


            if ($profilePic == 1) {

                $update1 = $conn->query("UPDATE user SET `picture` = '$path' WHERE `iduser` = " . $iduser . " ");
                $_SESSION['photo'] = $path;

                if (($conn->query($update1)) === TRUE) {
                    echo "New record created successfully";

                } else {
                    //echo "Erreur";
                }
                echo '<script type="text/javascript">
                                window.location.replace("../profile.php")
                                </script>';

            }

            if ($profilePic == 2) {

                $update = $conn->query("UPDATE user SET `cover` = '$path' WHERE `iduser` = " . $iduser . " ");
                $_SESSION['cover'] = $path;

                if (($conn->query($update)) === TRUE) {
                    echo "New record created successfully";

                } else {
                    //echo "Erreur";
                }
                echo '<script type="text/javascript">
                                window.location.replace("../profile.php")
                                </script>';

            }




            mysqli_close($conn);

        } else {
            echo "File not in the right format";
        }
    } else //Si pas de fichier, Juste publication
    {
        //Cree juste la publication
        require_once('createPublication.php');
    }

} else {
    echo "Error REQUEST_METHOD";
}


?>