<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="css/interface.css"/>


    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway - Settings</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">


    <!-- Navigation -->
    <?php include('navigation.php'); ?>


    <div id="page-wrapper">

        <div class="well publication">
            <h2> Modify your informations : </h2>
            <form enctype="multipart/form-data" action="scripts/modifier.php" method="post">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Pseudo </label>
                            <input class="form-control" placeholder="Enter your new Pseudo" name="newPseudo"
                                   id="newPseudo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" placeholder="Enter your new email" name="newEmail"
                                   id="newEmail">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" placeholder="Enter your description" name="newDescription"
                                      id="newDescription"></textarea>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <input type="submit" value="Modify" class="btn btn-default">
                        </div>
                    </div>
                </div>

            </form>
        </div>

        <div class="well publication">
            <h2> Change your profile picture ! <?php
                echo ' <IMG SRC =' . $_SESSION['photo'] . ' class="profile_pic2"/>';
                ?></h2>

            <form method="post" action="scripts/upload.php" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>File</label>
                            <input class="form-control" type="file" name="file" id="files" value="">
                        </div>
                    </div>
                </div>

                <input type="hidden" name="type" value="profilepicture"/>
                <input type="hidden" name="pic" value="1"/>

                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" type="hidden" name="content" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" type="hidden" name="location" value=""
                                   placeholder="Enter your new Pseudo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input type="hidden" class="form-control" placeholder="feeling" name="feeling" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" placeholder="access" type="hidden" name="access" value="" ">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <input type="submit" value="Modify" class="btn btn-default">
                        </div>
                    </div>
                </div>

            </form>

        </div>

        <div class="well publication">
            <h2> Change your cover picture ! <?php
                echo ' <IMG SRC =' . $_SESSION['cover'] . ' class="profile_pic2"/>';
                ?></h2>

            <form method="post" action="scripts/upload.php" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>File</label>
                            <input class="form-control" type="file" name="file" id="files" value="">
                        </div>
                    </div>
                </div>

                <input type="hidden" name="type" value="coverpicture"/>
                <input type="hidden" name="pic" value="2"/>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" type="hidden" name="content" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" type="hidden" name="location" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input type="hidden" class="form-control" placeholder="feeling" name="feeling" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label> </label>
                            <input class="form-control" placeholder="access" type="hidden" name="access" value="" ">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <input type="submit" value="Modify" class="btn btn-default">
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <?php
        include('footer.html');
        ?>
    </div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>
</html>