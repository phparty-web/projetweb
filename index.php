<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway - Connexion</title>

    <!-- Personnal CSS -->
    <link href="css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="js/jquery.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/txtHint.js"></script>

</head>

<body class="background">
<h1 class="center">
    Sway
</h1>
<br>
<div id="wrapit">
    <div class="col-lg-12">
        <!-- INSCRIPTION -->
        <div class="col-md-5">
            <form role="form" action="scripts/addUser.php" method="post" class="formito">
                <h1 class="center">REGISTER</h1>
                <div class="row">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input class="form-control" placeholder="Enter your last name" name="name" id="name">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Pseudo</label>
                        <input class="form-control" placeholder="Enter your pseudo" name="pseudo" id="pseudo">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" placeholder="Enter your email" name="email" id="email">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" name="pass1" id="pass1"
                               onkeyup="showHint(this.value)">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Please confirm your password</label>
                        <input class="form-control" type="password" name="pass2" id="pass2">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="actions">
                            <input type="submit" value="Register" class="btn btn-default">
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-2"></div>
        <!-- CONNEXION -->
        <div class="col-md-5">
            <form role="form" action="scripts/connect.php" method="post" class="formito">
                <h1 class="center">CONNECT</h1>
                <br>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="form-group">
                        <br>
                        <label>Email</label>
                        <input class="form-control" placeholder="Enter your email" name="email" id="email">

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password" id="password">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <input type="submit" value="Connect" class="btn btn-default">
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </form>
        </div>
        <!-- <script src="js/disable.js"></script> -->
    </div>
</div>
<!-- /#wrapper -->


<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
