$(document).ready(function () {
    $('.form-group input').keyup(function () {

        var empty = false;
        $('.form-group input').each(function () {
            if ($(this).val().length === 0) {
                empty = true;
            }
        });

        if (empty) {
            $('.actions input').attr('disabled', 'disabled');
        } else {
            $('.actions input').attr('disabled', false);
        }
    });
});
/**
 * Created by Arthur on 21/04/2017.
 */
