/**
 * Created by Arthur on 16/04/2017.
 */

function showHint(str) {
    if (str.length == 0) {
        document.getElementById("txtHint").innerHTML = "";

    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "../scripts/gethint.php?q=" + str, true);
        xmlhttp.send();
    }
}