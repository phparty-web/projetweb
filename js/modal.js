/**
 * Created by Arthur on 18/04/2017.
 */
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
    modal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

$(document).ready(function () {
    $('.form-group input').keyup(function () {

        var empty = false;
        $('.form-group input').each(function () {
            if ($(this).val().length === 0) {
                empty = true;
            }
        });

        if (empty) {
            $('.actions input').attr('disabled', 'disabled');
        } else {
            $('.actions input').attr('disabled', false);
        }
    });
});

// Get the <span> element that closes the modal
var addEndDate = document.getElementsByClassName("endDate-form")[0];

// Get the <span> element that closes the modal
var addLink = document.getElementsByClassName("addLink")[0];

function showEndDate() {
    addEndDate.style.display = "block";
    addLink.style.display = "none";
}

function HideEndDate() {
    addEndDate.style.display = "none";
    addLink.style.display = "block";
}