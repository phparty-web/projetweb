<!DOCTYPE html>
<?php
session_start();
//DELETE CACHE
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway</title>

    <!-- Personnal CSS -->
    <link href="css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include('navigation.php'); ?>

    <div id="page-wrapper">

        <div class="container-fluid">
            <div class="well publication">
                <form enctype="multipart/form-data" action="scripts/upload.php" method="post">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php
                                $photo = $_SESSION['photo'];
                                echo ' <IMG SRC =' . $photo . ' class="profile_pic2"/> ';
                                ?>
                                <label>Content </label><i class="fa fa-fw fa-paper-plane"></i>
                                <textarea rows="5" cols="50" class="form-control" type="text" name="content" value=""
                                          placeholder="Enter your post"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Location </label><i class="fa fa-fw fa-map-marker"></i>
                                <input class="form-control" type="text" name="location" value=""
                                       placeholder="Enter your location">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> Feeling</label><i class="fa fa-fw fa-smile-o"></i>
                                <select class="form-control" name="feeling">

                                    <option value="0"> Content :)</option>
                                    <option value="1"> Emerveillé :o</option>
                                    <option value="2">Fatigué :/</option>
                                    <option value="3"> Enervé :'(</option>


                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Access </label><i class="fa fa-fw fa-lock"></i>
                                <select class="form-control" name="access">
                                <option value="0"> Public</option>
                                <option value="1"> Mes amis seulement</option>
                                <option value="2">Uniquement moi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <input type="hidden" name="type" value="upload"/>
                                <label>File </label><i class="fa fa-fw fa-file"></i>

                                <input type="file" name="file" value="Choose a file" id="file"/>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <input type="submit" value="Publish" class="btn btn-warning">
                            </div>
                        </div>
                    </div>

                </form>
            </div>


            <?php

            $GLOBALS['typeFeed'] = "newsfeed";
            $GLOBALS['id'] = $_SESSION["id"];
            $GLOBALS['name'] = $_SESSION["name"];
            $GLOBALS['photo'] = $photo;

            require_once("scripts/publications.php");
            ?>

        </div>
        <!-- /.container-fluid -->
        <?php
        include('footer.html');
        ?>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
