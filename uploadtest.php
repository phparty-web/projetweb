<!DOCTYPE html>
<?php
session_start();
//DELETE CACHE
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/ic_loopr.ico">

    <title>Sway</title>

    <!-- Personnal CSS -->
    <link href="css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include('navigation.php'); ?>

    <div id="page-wrapper">

        <div class="container-fluid">
            <h1>Welcome to Sway !</h1>

            <div class="well publication">
                <form enctype="multipart/form-data" action="scripts/upload.php" method="post">


                    content <input type="text" name="content" value=""/> <br>
                    location <input type="text" name="location" value=""/> <br>
                    feeling<input type="number" name="feeling" value=""/> <br>
                    access<input type="number" name="access" value=""/>
                    <input name="file" type="file" id="files"/> <br>
                    <input type="hidden" name="type" value="upload"/>

                    <input type="submit" value="Publish"/>
                </form>
            </div>

            <output id="list"></output>

            <script>
                function handleFileSelect(evt) {
                    var files = evt.target.files; // FileList object

                    var output = [];
                    for (var i = 0, f; f = files[i]; i++) {
                        output.push('<li><strong>',
                            //escape(f.name),
                            '</strong> (', f.type || 'n/a', ') - ',
                            f.size, ' bytes, last modified: ',
                            f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                            '</li>');
                    }
                    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
                }

                document.getElementById('files').addEventListener('change', handleFileSelect, false);
            </script>

        </div>
        <!-- /.container-fluid -->
        <?php
        include('footer.html');
        ?>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>


