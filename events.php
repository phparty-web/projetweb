<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 18/04/2017
 * Time: 17:29
 */
session_start(); ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway - Events</title>

    <!-- Personnal CSS -->
    <link href="css/interface.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- jQuery -->
    <script src="js/jquery.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include('navigation.php'); ?>


    <div id="page-wrapper">
        <h2>My Events</h2>

        <!-- Trigger/Open The Modal -->
        <button id="myBtn" class="btn btn-primary">Create Event</button>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2>Create an Event</h2>
                    Please fill every form.

                </div>
                <div class="modal-body">
                    <form role="form" action="scripts/createEvent.php" method="post">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Name</label>*
                                    <input class="form-control" placeholder="Enter your event name" name="name"
                                           id="name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Location</label>*
                                    <input class="form-control" placeholder="Enter your location" name="location"
                                           id="location">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Start Date</label>*
                                    <input class="form-control" type="datetime-local" name="eventStart" id="eventStart">
                                    <script>
                                        $('input[id="eventStart"]').prop('valueAsNumber', Math.floor(new Date() / 60000) * 60000 + 3600000 * 2);
                                    </script>
                                </div>
                            </div>
                            <a class="addLink" onclick="showEndDate()" href="#">+ Add End Date</a>
                            <div class="col-sm-6">
                                <div class="endDate-form">
                                    <label>End Date </label><i class="fa fa-fw fa-times" onclick="HideEndDate()"></i>
                                    <input class="form-control" type="datetime-local" name="eventEnd" id="eventEnd"
                                           onclick="defaultEndValue()">
                                    <script>
                                        function defaultEndValue() {
                                            $('input[id="eventEnd"]').prop('valueAsNumber', Math.floor(new Date() / 60000) * 60000 + 3600000 * 4); // 60seconds * 1000milliseconds
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class='actions'>
                                    <input type="submit" value="Create" class="btn btn-primary" disabled="disabled">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /.Modal -->

        <div class="eventList">
            <br>
            <?php
            require_once('scripts/getEvent.php');
            ?>
            <div id="myModal2" class="modal2">
                <!-- Modal content -->
                <div class="modal2-content">
                    <div class="modal2-header">
                        <span class="close2">&times;</span>
                        <h2>Invite your friends</h2>
                    </div>
                    <div class="modal2-body">
                        <?php require_once('scripts/getFriends.php');

                        echo "</div>";
                        echo "<div class=\"modal-footer\">
                        <input type=\"hidden\" value=" . $idEv . " name =\"idevent\">
                        <button type=\"submit\" class=\"invBtn btn btn-primary\"><i class=\"fa fa-fw fa-share\"></i> Invite</button>
                    </form>
                    </div>" ?>
                </div>
            </div>
            <!-- /.Modal -->
                <script src="js/modal2.js"></script>
        </div>
            <?php
            include('footer.html');
            ?>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Modal -->
    <script src="js/modal.js"></script>

<!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
