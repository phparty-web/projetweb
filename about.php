<!DOCTYPE html>
<?php session_start();

if (isset($_GET["ref"]) && !empty($_GET["ref"])) {

    $id_user = $_GET["ref"];
    require_once('scripts/dbConnect.php');

    $sql = "Select * from `user` where `iduser` ='" . $id_user . "'";
    $tab = mysqli_fetch_array(mysqli_query($conn, $sql));

    $result = array();
    array_push($result,
        array("id" => $tab['iduser'],
            "name" => $tab['name'],
            "pseudo" => $tab['pseudo'],
            "email" => $tab['email'],
            "password" => $tab['password'],
            "picture" => $tab['picture'],
            "cover" => $tab['cover'],
            "description" => $tab['description'],
            "type" => $tab['type']));

    $name = $tab['name'];
    $pseudo = $tab['pseudo'];
    $email = $tab['email'];
    $photo = $tab['picture'];
    $photo2 = $tab['cover'];
    $description = $tab['description'];
    $type = $tab['type'];

} else {
    $id_user = $_SESSION['id'];
    $name = $_SESSION['name'];
    $pseudo = $_SESSION['pseudo'];
    $email = $_SESSION['email'];
    $photo = $_SESSION['photo'];
    $photo2 = $_SESSION['cover'];
    $description = $_SESSION['description'];
}

?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="css/interface.css"/>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <link rel="icon" type="image/x-icon" href="img/ic_loopr.ico">

    <title>Sway - About</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">


    <!-- Navigation -->
    <?php include('navigation.php'); ?>


    <div id="page-wrapper">
        <div class="thewrapper">


            <?php

            echo "<div class='jumbotron2' >";
            echo "<span class=\"name3\">" . $name . "</span>";
            echo ' <IMG SRC =' . $photo2 . ' class="cover_pic"  />';
            echo ' <IMG SRC =' . $photo . ' class="profile_pic"/> ';

            echo "</div>"; ?>

            <div class="menu">
                <div class="navbar navbar-inverse">
                    <div class="container">

                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a class="profile-link" href="">Home</a>
                                </li>
                                <li class="active"><a class="about-link" href="">About</a>
                                </li>
                                <li><a class="friend-link" href="">Friends</a>
                                </li>
                                <li><a class="photo-link" href="">Pictures</a>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <div class="publication well">

                <?php
                require_once("scripts/about.php");
                ?>
            </div>
        </div>

        <script>
            $(".profile-link").attr("href", "profile.php?ref=" + "<?php echo($id_user);?>");
            $(".about-link").attr("href", "about.php?ref=" + "<?php echo($id_user);?>");
            $(".friend-link").attr("href", "friends.php?ref=" + "<?php echo($id_user);?>");
            $(".photo-link").attr("href", "photos.php?ref=" + "<?php echo($id_user);?>");
        </script>


        <?php
        include('footer.html');
        ?>
    </div>


    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
