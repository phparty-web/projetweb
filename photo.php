<!DOCTYPE html>
<?php
session_start();
//DELETE CACHE
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


if (isset($_GET["ref"]) && !empty($_GET["ref"])) {
    $iduser = $_GET["ref"];
} else {
    $iduser = $_SESSION['id'];
}


?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/x-icon" href="img/logo/ic_loopr.ico">

    <title>Sway - Pictures</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/interface.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include('navigation.php'); ?>

    <div id="page-wrapper">

        <div class="container-fluid">
            <h2> Album</h2>

            <form role="form" action="scripts/addAlbum.php" method="post">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" placeholder="Enter your album name" name="nomAlbum" id="nomAmi">

                        </div>
                    </div>
                </div>


        </div>

        <?php
        $taille = 0;
        require_once("scripts/dbConnect.php");
        $photos = $conn->query('SELECT * FROM upload WHERE id_user = ("' . $iduser . '") ');

        while ($donnees = mysqli_fetch_assoc($photos)) {
            $demande = $donnees['path'];
            $numero = $donnees['id'];
            $taille++;
            echo "<input type='checkbox' name ='id[]' value=" . $numero . "> <img class=\" album_pic\" src=" . $demande . " >";
            echo "<input type='hidden' name ='taille' value=" . $taille . ">";
        }

        ?>

        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-default">
                </div>
            </div>
        </div>
        </form>


        <!-- /.container-fluid -->
        <?php
        include('footer.html');
        ?>
    </div>
    <!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
